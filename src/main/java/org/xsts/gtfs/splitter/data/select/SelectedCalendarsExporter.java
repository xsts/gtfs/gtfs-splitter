package org.xsts.gtfs.splitter.data.select;

import org.xsts.gtfs.gds.data.collections.GTFSCalendarLinearList;
import org.xsts.gtfs.gds.data.types.GTFSCalendar;

import java.util.ArrayList;
import java.util.List;
/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class SelectedCalendarsExporter  extends Exporter {
    public static final String FILE_NAME = "calendar.txt";

    public SelectedCalendarsExporter(String dirPath) {
        super(dirPath);
    }

    public boolean export(GTFSCalendarLinearList calendars)  {
        List<String> textRows = new ArrayList<>();
        StringBuilder lineBuilder = new StringBuilder();

        lineBuilder.append("service_id")
                .append(",").append("monday")
                .append(",").append("tuesday")
                .append(",").append("wednesday")
                .append(",").append("thursday")
                .append(",").append("friday")
                .append(",").append("saturday")
                .append(",").append("sunday")
                .append(",").append("start_date")
                .append(",").append("end_date");

        textRows.add(lineBuilder.toString());
        for ( GTFSCalendar service : calendars.getAll()){

            lineBuilder = new StringBuilder();
            lineBuilder.append(service.service())
                    .append(",").append((service.monday()))
                    .append(",").append((service.tuesday()))
                    .append(",").append((service.wednesday()))
                    .append(",").append((service.thursday()))
                    .append(",").append((service.friday()))
                    .append(",").append((service.saturday()))
                    .append(",").append((service.sunday()))
                    .append(",").append(service.start())
                    .append(",").append(service.end());

            textRows.add(lineBuilder.toString());
        }

        return writeListToFile(FILE_NAME, textRows);
    }

    private String numeric(boolean value) {
        if ( value)
            return "1";
        return "0";
    }


}
