package org.xsts.gtfs.splitter.data.select;


import org.xsts.gtfs.gds.data.collections.*;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class SelectExporter {
    GTFSAgencyList agencies;
    GTFSRouteList routes;
    GTFSTripList trips;
    GTFSStopLinearList stops;
    GTFSStopTimeLinearList stopTimes;
    GTFSCalendarLinearList calendars;

    Integer index;
    String exportDir;

    public SelectExporter() {

    }

    boolean isValid() {
        if (  agencies.isEmpty())
            return false;
        if (routes.isEmpty())
            return false;
        if (trips.isEmpty())
            return false;
        if (stops.isEmpty())
            return false;
        if (stopTimes.isEmpty())
            return false;
        if (calendars.isEmpty())
            return false;
        return true;
    }

    public SelectExporter index(Integer index) {
        this.index = index;
        return this;
    }

    public SelectExporter exportDir(String exportDir) {
        this.exportDir = exportDir;
        return this;
    }

    public SelectExporter agencies(GTFSAgencyList agencies) {
        this.agencies = agencies;
        return this;
    }

    public SelectExporter routes(GTFSRouteList routes) {
        this.routes = routes;
        return this;
    }

    public SelectExporter trips(GTFSTripList trips) {
        this.trips = trips;
        return this;
    }

    public SelectExporter stops(GTFSStopLinearList stops) {
        this.stops = stops;
        return this;
    }

    public SelectExporter stopTimes(GTFSStopTimeLinearList stopTimes) {
        this.stopTimes = stopTimes;
        return this;
    }

    public SelectExporter calendars(GTFSCalendarLinearList calendars) {
        this.calendars = calendars;
        return this;
    }


    public boolean export() {

        //GTFSStopTimeLinearList stopTimes;

        if (isValid() == false) {
            System.out.println("The data set is invalid and thus it will not be created" );
            return false;
        }

        File folder =  new File(exportDir);
        if ( !folder.exists())
            folder.mkdir();

        Path pathToDir  = Paths.get(exportDir, index.toString());
        folder =  new File(pathToDir.toString());
        if ( !folder.exists())
            folder.mkdir();

        new SelectedAgenciesExporter(pathToDir.toString()).export(agencies);
        new SelectedRoutesExporter(pathToDir.toString()).export(routes);
        new SelectedStopsExporter(pathToDir.toString()).export(stops);
        new SelectedCalendarsExporter(pathToDir.toString()).export(calendars);
        new SelectedTripsExporter(pathToDir.toString()).export(trips);
        new SelectedStopTimesExporter(pathToDir.toString()).export(stopTimes);

        return true;
    }



}