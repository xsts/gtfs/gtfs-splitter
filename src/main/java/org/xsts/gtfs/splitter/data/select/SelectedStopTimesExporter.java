package org.xsts.gtfs.splitter.data.select;

import org.xsts.gtfs.gds.data.collections.GTFSStopTimeLinearList;
import org.xsts.gtfs.gds.data.types.GTFSStopTime;

import java.util.ArrayList;
import java.util.List;
/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class SelectedStopTimesExporter  extends Exporter {
    public static final String FILE_NAME = "stop_times.txt";

    public SelectedStopTimesExporter(String dirPath) {
        super(dirPath);
    }

    public boolean export(GTFSStopTimeLinearList stopTimes)  {
        List<String> textRows = new ArrayList<>();
        StringBuilder lineBuilder = new StringBuilder();

        lineBuilder.append("trip_id")
                .append(",").append("arrival_time")
                .append(",").append("departure_time")
                .append(",").append("stop_id")
                .append(",").append("stop_sequence")
                .append(",").append("stop_headsign")
                .append(",").append("shape_dist_traveled");


        textRows.add(lineBuilder.toString());
        for ( GTFSStopTime stopTime : stopTimes.getAll()){

            lineBuilder = new StringBuilder();
            lineBuilder.append(stopTime.trip())
                    .append(",").append(stopTime.arrival())
                    .append(",").append(stopTime.departure())
                    .append(",").append(stopTime.stop())
                    .append(",").append(stopTime.sequence())
                    .append(",")//.append(stopTime.headsign())
                    .append(",");//.append(stopTime.shape());
            textRows.add(lineBuilder.toString());
        }

        return writeListToFile(FILE_NAME, textRows);
    }
}