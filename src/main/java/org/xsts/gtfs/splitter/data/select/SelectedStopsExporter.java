package org.xsts.gtfs.splitter.data.select;

import org.xsts.gtfs.gds.data.collections.GTFSStopLinearList;
import org.xsts.gtfs.gds.data.types.GTFSStop;

import java.util.ArrayList;
import java.util.List;
/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class SelectedStopsExporter extends Exporter {
    public static final String FILE_NAME = "stops.txt";

    public SelectedStopsExporter(String dirPath) {
        super(dirPath);
    }

    public boolean export(GTFSStopLinearList stops)  {
        List<String> textRows = new ArrayList<>();
        StringBuilder lineBuilder = new StringBuilder();


        lineBuilder.append("stop_id")
                .append(",").append("stop_code")
                .append(",").append("stop_name")
                .append(",").append("stop_desc")
                .append(",").append("stop_lat")
                .append(",").append("stop_lon")
                .append(",").append("location_type")
                .append(",").append("parent_station");


        textRows.add(lineBuilder.toString());
        for ( GTFSStop stop : stops.getAll()){

            lineBuilder = new StringBuilder();
            lineBuilder.append(stop.id())
                    .append(",").append(stop.code())
                    .append(",").append(quote(QuoteRemover.removeQuote(stop.name())))
                    .append(",") .append(quote(QuoteRemover.removeQuote(stop.description()))) // Added stop desc
                    .append(",").append(stop.latitude())
                    .append(",").append(stop.longitude());
            lineBuilder.append(",").append("0").append(",");


            textRows.add(lineBuilder.toString());
        }

        return writeListToFile(FILE_NAME, textRows);
    }
}