# GTFS splitter

Some GTFS feeds come with dozens of agencies bundled together. Loading them together would be both memory and time consuming. The splitter is a tool that separates the feeds by agency.

One of the main problems I encountered was to separate personal data from generic. While I am still pondering on the topic, it is possible, to bypass it by using a shared folder. 

After splitting the data, I was able to generate very fast the schematics for three dozen small GTFS datasets. The initial bundle was quite big. 

Currently, the splitter handles the following files: 

- agency.txt
- calendar.txt
- routes.txt
- stops.txt
- stop-times.txt
- trips.txt

The other files will be simply ignored.

You will need the GDS (GTFS data type) and GDL (file loading) projects in order to compile the splitter. Currently they are not publicly available. The culprit is the everchanging nature of the GTFS. I still have to implement types and loaders for some of the newer .txt files. 

Note that  I could create a similar joiner project. That will not be the case.  The size of the combined data would make its use prohibitive.