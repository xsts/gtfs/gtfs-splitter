package org.xsts.gtfs.splitter.data.select;

import org.xsts.gtfs.gds.data.collections.GTFSRouteList;
import org.xsts.gtfs.gds.data.types.GTFSRoute;

import java.util.ArrayList;
import java.util.List;
/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class SelectedRoutesExporter extends Exporter {
    public static final String FILE_NAME = "routes.txt";

    public SelectedRoutesExporter(String dirPath) {
        super(dirPath);
    }

    public boolean export(GTFSRouteList routes)  {
        List<String> textRows = new ArrayList<>();
        StringBuilder lineBuilder = new StringBuilder();

        lineBuilder.append("route_id")
                .append(",").append("agency_id")
                .append(",").append("route_short_name")
                .append(",").append("route_long_name")
                .append(",").append("route_desc")
                .append(",").append("route_type")
                .append(",").append("route_url")
                .append(",").append("route_color")
                .append(",").append("route_text_color");


        textRows.add(lineBuilder.toString());
        for ( String id : routes.keySet()){
            GTFSRoute route = routes.get(id);
            lineBuilder = new StringBuilder();
            lineBuilder.append(route.id())
                    .append(",").append(route.agency())
                    .append(",").append(route.shortName())
                    .append(",").append(quote(route.longName()))
                    // stra,ge description. better stick with the long name
                    .append(",").append(quote(route.longName()))
                    //.append(",").append(quote(route.description()))
                    .append(",").append(route.type());
            lineBuilder.append(",");
            if ( route.url() != null)
                lineBuilder.append(route.url());
            lineBuilder.append(",");
            if ( route.color() != null)
                lineBuilder.append(route.color());
            lineBuilder.append(",");
            if ( route.textColor() != null)
                lineBuilder.append(route.textColor());

            textRows.add(lineBuilder.toString());
        }

        return writeListToFile(FILE_NAME, textRows);
    }
}