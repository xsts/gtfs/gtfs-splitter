package org.xsts.gtfs.splitter.data.select;

import org.xsts.gtfs.gds.data.collections.GTFSAgencyList;
import org.xsts.gtfs.gds.data.types.GTFSAgency;

import java.util.ArrayList;
import java.util.List;
/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class SelectedAgenciesExporter extends Exporter {
    public static final String FILE_NAME = "agency.txt";

    public SelectedAgenciesExporter(String dirPath) {
        super(dirPath);
    }

    public boolean export(GTFSAgencyList agencies)  {
        List<String> textRows = new ArrayList<>();
        StringBuilder lineBuilder = new StringBuilder();
        lineBuilder.append("agency_id")
                .append(",").append("agency_name")
                .append(",").append("agency_url")
                .append(",").append("agency_timezone")
                .append(",").append("agency_lang")
                .append(",").append("agency_phone");

        textRows.add(lineBuilder.toString());
        for ( String id : agencies.keySet()){
            GTFSAgency agency = agencies.get(id);
            lineBuilder = new StringBuilder();
            lineBuilder.append(agency.id())
                    .append(",").append(agency.name())
                    .append(",").append(agency.url())
                    .append(",").append(agency.timezone());
            lineBuilder.append(",");
            if ( agency.lang() != null)
                lineBuilder.append(agency.lang());
            lineBuilder.append(",");
            if ( agency.phone() != null)
                lineBuilder.append(agency.phone());
            textRows.add(lineBuilder.toString());
        }

        return writeListToFile(FILE_NAME, textRows);
    }
}
