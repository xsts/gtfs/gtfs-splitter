package org.xsts.gtfs.splitter.data.select;

import org.xsts.gtfs.gds.data.collections.GTFSTripList;
import org.xsts.gtfs.gds.data.types.GTFSTrip;

import java.util.ArrayList;
import java.util.List;
/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class SelectedTripsExporter  extends Exporter {
    public static final String FILE_NAME = "trips.txt";

    public SelectedTripsExporter(String dirPath) {
        super(dirPath);
    }

    public boolean export(GTFSTripList trips)  {
        List<String> textRows = new ArrayList<>();
        StringBuilder lineBuilder = new StringBuilder();

        lineBuilder.append("route_id")
                .append(",").append("service_id")
                .append(",").append("trip_id")
                .append(",").append("trip_headsign")
                .append(",").append("trip_short_name")
                .append(",").append("direction_id")
                .append(",").append("shape_id");

        textRows.add(lineBuilder.toString());


        for ( String key1: trips.keySet()){
            for ( String key2: trips.keySet2(key1)) {
                for ( String key3 : trips.keySet3(key1, key2)){
                    GTFSTrip trip = (GTFSTrip)trips.get(key1,key2,key3);
                    lineBuilder = new StringBuilder();
                    lineBuilder.append(trip.route())
                            .append(",").append(trip.service())
                            .append(",").append(trip.id())
                            .append(",").append(trip.headsign())
                            .append(",")// .append(trip.shortName())
                            .append(",") //.append(trip.direction())
                            .append(",")
                    ;
                    textRows.add(lineBuilder.toString());
                }
            }
        }


        return writeListToFile(FILE_NAME, textRows);
    }

    private String numeric(boolean value) {
        if ( value)
            return "1";
        return "0";
    }


}