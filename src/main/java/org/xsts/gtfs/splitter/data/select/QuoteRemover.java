package org.xsts.gtfs.splitter.data.select;
/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class QuoteRemover {
    public static String removeQuote(String text) {
        if (text == null)
            return null;
        String[] tok = text.split("\"");
        StringBuilder builder = new StringBuilder();
        for ( String s: tok){
            builder.append(s);
        }
        return builder.toString();
    }
}
