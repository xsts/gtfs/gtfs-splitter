package org.xsts.gtfs.splitter.executors;

import org.xsts.core.config.EnvironmentConfiguration;
import org.xsts.core.config.update.FileSystemUpdater;
import org.xsts.core.data.collections.LinearValueList;
import org.xsts.core.data.util.TextFileLineCounter;
import org.xsts.gtfs.gdl.loaders.BasicDirLoader;
import org.xsts.gtfs.gdl.loaders.processors.csv.agency.GTFSAgencyProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.calendar.GTFSCalendarSelectionProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.route.GTFSRouteByAgencyIDProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.stop.GTFSStopSelectionProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.stoptime.GTFSStopTimeSelectionProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.trip.GTFSTripByRouteListProcessor;
import org.xsts.gtfs.gds.data.collections.*;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;
import org.xsts.gtfs.gds.data.names.GTFSFileNames;
import org.xsts.gtfs.gds.data.types.GTFSAgency;
import org.xsts.gtfs.gds.data.types.GTFSRoute;
import org.xsts.gtfs.gds.data.types.GTFSTrip;
import org.xsts.gtfs.splitter.data.select.SelectExporter;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class SPLExecutor {
    protected List<String> cmdLineArgs;
    protected EnvironmentConfiguration config;
    GTFSAgencyList agencies;
    GTFSRouteList routes;
    GTFSTripList trips;
    GTFSStopLinearList stops;
    GTFSStopTimeLinearList stopTimes;
    GTFSCalendarLinearList calendars;

    String exportDir;
    Integer index;

    // So far, it is hardcoded. This could be set via command line
    String DATASET_NAME = "dataset-name";

    String inputDir;
    String outputDir;

    public SPLExecutor() {
        config = EnvironmentConfiguration.getInstance();
        inputDir = config.getString(FileSystemUpdater.DATA_INPUT_DIR);// + "/" + DATASET_NAME;
        outputDir = config.getString(FileSystemUpdater.DATA_OUTPUT_DIR);// + "/" + DATASET_NAME;
    }

    public SPLExecutor(String args[]) {
        cmdLineArgs = new ArrayList<>();
        for (String arg : args){
            cmdLineArgs.add(arg);
        }
        init();
    }

    public void init() {
        initConfig();

    }


    public void initConfig() {
        config = EnvironmentConfiguration.getInstance();
        initFileSystem();
    }

    public void initFileSystem() {
        // Feel free to change this path to something suitable for your project
        String homeDir = System.getProperty("user.home");
        String separator = System.getProperty("file.separator");
        StringBuilder xstsPath = new StringBuilder();
        xstsPath.append(homeDir)
                .append(separator)
                .append("projects")
                .append(separator)
                .append("xsts");
        config.source(new FileSystemUpdater())
                .put(FileSystemUpdater.ROOT_DIR, xstsPath.toString())
                .put(FileSystemUpdater.DATA_INPUT_DIR, "${ROOT_DIR}/input/bulk")
                .put(FileSystemUpdater.DATA_OUTPUT_DIR, "${ROOT_DIR}/output/bulk");

        inputDir = config.getString(FileSystemUpdater.DATA_INPUT_DIR); // + "/" + DATASET_NAME;
        outputDir = config.getString(FileSystemUpdater.DATA_OUTPUT_DIR); // + "/" + DATASET_NAME;

    }

    public void load( ) {
        int cutoff = index;
        loadAgencies();
        System.out.println("Found " + agencies.count() + " agencies");
        selectAgency(cutoff);
        //printAgencies();

        loadRoutes(selectedAgencyID());
        //printRoutes();
        loadTrips(routes);
        //printTrips();
        loadStopTimes();
        loadCalendar();

        loadCalendarDates();
        loadStops();

    }

    private void loadStopTimes() {
        System.out.println("loadStopTimes begin");


        Path pathToDir = Paths.get(inputDir);
        Set<String> tripKeys = trips.tripIDList();
        Integer lineCountStopTimes = TextFileLineCounter.countLines(pathToDir,
                GTFSFileNames.STOP_TIMES);
        LinearValueList lodList;
        lodList =  BasicDirLoader.load(pathToDir,
                GTFSFileNames.STOP_TIMES,
                new GTFSStopTimeLinearList(),
                new GTFSStopTimeSelectionProcessor(),
                tripKeys,
                lineCountStopTimes);
        stopTimes = (GTFSStopTimeLinearList) lodList;
        System.out.println("loadStopTimes end");

    }

    public void run() {
        //app.run();
    }


    private void loadCalendarDates() {
        // TBD later
    }

    private void loadCalendar() {
        System.out.println("loadCalendar begin");
        Path pathToDir = Paths.get(inputDir);
        Set<String> serviceKeys = trips.serviceIDList();
        Integer lineCountCalendar = TextFileLineCounter.countLines(pathToDir,
                GTFSFileNames.CALENDAR);
        LinearValueList lodList;
        lodList =  BasicDirLoader.load(pathToDir,
                GTFSFileNames.CALENDAR,
                new GTFSCalendarLinearList(),
                new GTFSCalendarSelectionProcessor(),
                serviceKeys,
                lineCountCalendar);
        calendars = (GTFSCalendarLinearList) lodList;

        System.out.println("loadCalendar end");

    }


    private void filterStops() {
    }

    private void loadStops() {
        System.out.println("loadStops begin");
        Set<String> stopKeys = stopTimes.stopIDList();
        Path pathToDir = Paths.get(inputDir);
        Integer lineCount = TextFileLineCounter.countLines(pathToDir,
                GTFSFileNames.STOPS);
        stops = (GTFSStopLinearList)BasicDirLoader.load(pathToDir,
                GTFSFileNames.STOPS,
                new GTFSStopLinearList(),
                new GTFSStopSelectionProcessor(),
                stopKeys,
                lineCount);
        System.out.println("loadStops end");
    }

    private void loadTrips(GTFSRouteList routes) {
        System.out.println("loadTrips begin");

        Path pathToDir = Paths.get(inputDir);
        trips = (GTFSTripList) BasicDirLoader.load(pathToDir,
                GTFSFileNames.TRIPS,
                new GTFSTripList(),
                new GTFSTripByRouteListProcessor(),
                routes,
                null);
        System.out.println("loadTrips end");

    }

    public void printAgencies() {
        for ( String agencyID : agencies.keySet()){
            GTFSAgency agency = agencies.get(agencyID);
            System.out.println(agency);
        }
    }

    public void printTrips() {
        for ( String routeID : trips.keySet()){
            for ( String serviceID : trips.keySet2(routeID)) {
                for (String tripID : trips.keySet3(routeID,serviceID)) {
                    GTFSTrip trip   = trips.getTrip(routeID, serviceID, tripID);
                    System.out.println(trip);
                }
            }

        }
    }


    public void printRoutes() {
        for ( String routeID : routes.keySet()){
            GTFSRoute route = routes.get(routeID);
            System.out.println(route);
        }
    }

    private void loadAgencies() {
        System.out.println("loadAgencies begin");

        Path pathToDir = Paths.get(inputDir);
        agencies = (GTFSAgencyList) BasicDirLoader.load(pathToDir,
                GTFSFileNames.AGENCY,
                new GTFSAgencyList(),
                new GTFSAgencyProcessor(),
                false,
                null);
        System.out.println("loadAgencies end");

    }

    public int getAgencyCount() { return agencies.count(); }

    private void loadRoutes(String agencyID) {
        System.out.println("loadRoutes begin");

        Path pathToDir = Paths.get(inputDir);
        routes = (GTFSRouteList) BasicDirLoader.load(pathToDir,
                GTFSFileNames.ROUTES,
                new GTFSRouteList(),
                new GTFSRouteByAgencyIDProcessor(),
                agencyID,
                null);
        System.out.println("loadRoutes end");

    }


    public GTFSAgencyList selectAgency(int cutoff) {
        GTFSAgency selectedAgency = null;
        int pos = -1;
        for ( String agencyID: agencies.keySet()){
            pos++;
            selectedAgency = agencies.get(agencyID);
            if ( pos == cutoff)
                break;
        }
        GTFSAgencyList selectedList = new GTFSAgencyList();
        selectedList.add(selectedAgency.id(), selectedAgency);
        agencies = selectedList;
        return selectedList;

    }

    public String selectedAgencyID() {
        for ( String agencyID: agencies.keySet()){
            return  agencyID;
        }
        return null;
    }

    public SPLExecutor exportDir(String exportDir) {
        this.exportDir = exportDir;
        return this;
    }

    public SPLExecutor index(Integer index) {
        this.index = index;
        return this;
    }

    public Integer indexById(String id) {

        GTFSAgency selectedAgency = null;
        int pos = -1;
        for ( String agencyID: agencies.keySet()){
            pos++;
            selectedAgency = agencies.get(agencyID);
            if ( selectedAgency.id().compareTo(id) == 0) {
                return pos;
            }

        }

        return -1;
    }



    public void export() {
        SelectExporter exporter = new SelectExporter();
        exporter.index(index).exportDir(outputDir);
        exporter.agencies(agencies);
        exporter.routes(routes);
        exporter.stops(stops);
        exporter.calendars(calendars);
        exporter.trips(trips);
        exporter.stopTimes(stopTimes);

        exporter.export();
    }

    public static void main(String [] args) {
        SPLExecutor counter = new SPLExecutor(args);
        GTFSCache cache = new GTFSCache();
        cache.makeGlobal();
        counter.loadAgencies();
        int number = counter.getAgencyCount(); // not yet optimizing
        for ( int i =  0; i < 1000 && i < counter.getAgencyCount(); i++) {
            System.out.println("pos = " + i);
            SPLExecutor executor = new SPLExecutor();
            executor.index(i);
            executor.load();
            executor.export();
        }


    }
}
