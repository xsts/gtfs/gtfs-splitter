package org.xsts.gtfs.splitter.data.select;

import java.io.*;
import java.util.List;
/*
 * Group : XSTS/GTFS
 * Project : Splitter
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class Exporter {
    String dirPath = null;
    protected Exporter(String dirPath) {
        this.dirPath = dirPath;
    }

    protected boolean writeListToFile(String fileName, List<String> textLines)  {
        boolean result = false;
        try {
            File file = new File(dirPath, fileName);
            file.delete();
            FileOutputStream fos = new FileOutputStream(file);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            try (FileWriter fileWriter = new FileWriter(file)) {
                for ( String textLine :textLines ){
                    bw.write(textLine);
                    bw.newLine();
                }
                bw.close();
                result = true;
            } catch (IOException exception){
                System.out.println("Cannot save to file");
                exception.printStackTrace();
            }
        } catch( FileNotFoundException exception){
            exception.printStackTrace();
        }



        return result;

    }

    public String quote(String text){
        if (text == null)
            return "";
        return "\"" + text + "\"";
    }
}
